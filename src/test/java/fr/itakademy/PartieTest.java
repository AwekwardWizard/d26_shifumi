package fr.itakademy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PartieTest {

    private Partie partie;
    private Joueur joueur1;
    private Joueur joueur2;

    @BeforeEach
    public void init() {
        this.joueur1 = new Joueur("Jane");
        this.joueur2 = new Joueur("Jack");
        this.partie = new Partie(joueur1, joueur2);
    }

    @Test
    void should_be_error_p1() {
        joueur1.setChoix(1337);
        joueur2.setChoix(0);
        assertEquals("Error", partie.Match(joueur1, joueur2));
    }

    @Test
    void should_be_error_p2() {
        joueur1.setChoix(0);
        joueur2.setChoix(420);
        assertEquals("Error", partie.Match(joueur1, joueur2));
    }

    @Test
    void should_be_draw_rock() {
        joueur1.setChoix(0);
        joueur2.setChoix(0);
        assertEquals("Egalite !", partie.Match(joueur1, joueur2));
    }

    @Test
    void should_be_draw_scissors() {
        joueur1.setChoix(1);
        joueur2.setChoix(1);
        assertEquals("Egalite !", partie.Match(joueur1, joueur2));
    }

    @Test
    void should_be_draw_paper() {
        joueur1.setChoix(2);
        joueur2.setChoix(2);
        assertEquals("Egalite !", partie.Match(joueur1, joueur2));
    }

    @Test
    void should_be_win_p1_rock() {
        joueur1.setChoix(0);
        joueur2.setChoix(1);
        int scoreJ1 = joueur1.getScore();
        assertEquals("Victoire pour "+joueur1.getPrenom()+" !", partie.Match(joueur1, joueur2));
        assertEquals(scoreJ1+1, this.joueur1.getScore());
    }

    @Test
    void should_be_win_p1_scissors() {
        joueur1.setChoix(1);
        joueur2.setChoix(2);
        int scoreJ1 = joueur1.getScore();
        assertEquals("Victoire pour "+joueur1.getPrenom()+" !", partie.Match(joueur1, joueur2));
        assertEquals(scoreJ1+1, this.joueur1.getScore());
    }

    @Test
    void should_be_win_p1_paper() {
        joueur1.setChoix(2);
        joueur2.setChoix(0);
        int scoreJ1 = joueur1.getScore();
        assertEquals("Victoire pour "+joueur1.getPrenom()+" !", partie.Match(joueur1, joueur2));
        assertEquals(scoreJ1+1, this.joueur1.getScore());
    }

    @Test
    void should_be_win_p2_rock() {
        joueur1.setChoix(1);
        joueur2.setChoix(0);
        int scoreJ2 = joueur2.getScore();
        assertEquals("Victoire pour "+joueur2.getPrenom()+" !", partie.Match(joueur1, joueur2));
        assertEquals(scoreJ2+1, this.joueur2.getScore());
    }

    @Test
    void should_be_win_p2_scissors() {
        joueur1.setChoix(2);
        joueur2.setChoix(1);
        int scoreJ2 = joueur2.getScore();
        assertEquals("Victoire pour "+joueur2.getPrenom()+" !", partie.Match(joueur1, joueur2));
        assertEquals(scoreJ2+1, this.joueur2.getScore());
    }

    @Test
    void should_be_win_p2_paper() {
        joueur1.setChoix(0);
        joueur2.setChoix(2);
        int scoreJ2 = joueur2.getScore();
        assertEquals("Victoire pour "+joueur2.getPrenom()+" !", partie.Match(joueur1, joueur2));
        assertEquals(scoreJ2+1, this.joueur2.getScore());
    }
}