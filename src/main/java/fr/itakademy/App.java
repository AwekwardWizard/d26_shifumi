package fr.itakademy;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner donnees = new Scanner(System.in);
        System.out.println( "Bienvenue sur ce programme de shifumi !" );

        System.out.println("Nom du premier joueur : ");
        String nJoueur1 = donnees.nextLine();
        Joueur joueur1 = new Joueur(nJoueur1);

        System.out.println("Nom du deuxième joueur : ");
        String nJoueur2 = donnees.nextLine();
        Joueur joueur2 = new Joueur(nJoueur2);

        Partie partie = new Partie(joueur1, joueur2);

        System.out.println("Choix du premier joueur (0 pour pierre, 1 pour ciseaux, 2 pour papier) : ");
        int cJoueur1 = donnees.nextInt();
        partie.getJoueur1().setChoix(cJoueur1);

        System.out.println("Choix du deuxième joueur (0 pour pierre, 1 pour ciseaux, 2 pour papier) : ");
        int cJoueur2 = donnees.nextInt();
        partie.getJoueur2().setChoix(cJoueur2);

        partie.Match(joueur1, joueur2);
        donnees.close();
    }
}