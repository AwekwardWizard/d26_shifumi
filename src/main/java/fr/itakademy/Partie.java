package fr.itakademy;

public class Partie {
    
    private Joueur joueur1;
    private Joueur joueur2;

    public Partie(Joueur joueur1, Joueur joueur2){
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
    }

    public void setJoueur1(Joueur joueur1) {
        this.joueur1 = joueur1;
    }

    public Joueur getJoueur1() {
        return joueur1;
    }

    public void setJoueur2(Joueur joueur2) {
        this.joueur2 = joueur2;
    }

    public Joueur getJoueur2() {
        return joueur2;
    }

    /* 0 corresponds to rock
    // 1 corresponds to scissors
    // 2 corresponds to paper
    */ 
    public String Match(Joueur joueur1, Joueur joueur2){
        if(joueur1.getChoix() - joueur2.getChoix() == 0){
            System.out.println("Egalite !");
            return("Egalite !");
        }
        if(joueur1.getChoix() - joueur2.getChoix() == -1){
            joueur1.setScore(joueur1.getScore()+1);
            System.out.println("Victoire pour "+joueur1.getPrenom()+" !");
            return("Victoire pour "+joueur1.getPrenom()+" !");
        }
        if(joueur1.getChoix() - joueur2.getChoix() == -2){
            joueur2.setScore(joueur2.getScore()+1);
            System.out.println("Victoire pour "+joueur2.getPrenom()+" !");
            return("Victoire pour "+joueur2.getPrenom()+" !");
        }
        if(joueur1.getChoix() - joueur2.getChoix() == 1){
            joueur2.setScore(joueur2.getScore()+1);
            System.out.println("Victoire pour "+joueur2.getPrenom()+" !");
            return("Victoire pour "+joueur2.getPrenom()+" !");
        }
        if(joueur1.getChoix() - joueur2.getChoix() == 2){
            joueur1.setScore(joueur1.getScore()+1);
            System.out.println("Victoire pour "+joueur1.getPrenom()+" !");
            return("Victoire pour "+joueur1.getPrenom()+" !");
        }
        return("Error");
    }
}