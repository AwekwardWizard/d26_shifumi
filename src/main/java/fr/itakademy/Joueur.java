package fr.itakademy;

public class Joueur {
    
    private String prenom;
    private int choix;
    private int score;

    public Joueur(String prenom){
        this.prenom = prenom;
        this.choix = -999;
        this.score = 0;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setChoix(int choix) {
        if (choix > -1 && choix < 3){
            this.choix = choix;
        }
    }

    public int getChoix() {
        return choix;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}
